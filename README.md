# MSET Testing Framework

Provides basic testing functionalities for the MSET project.

## Prerequisites

* Eclipse
* Android SDK Manager
* Gradle

## Installation

1. Download the Android SDK and download the following packages using the `tools/android` package manager:
   1. `Android N (API 24) -> SDK Platform` 
2. In the `build.gradle` file edit the `ANDROID_HOME` variable to your local android sdk directory
3. Execute the `gradle eclipse` command to generate the eclipse project files & refresh your eclipse project

## Building the framework

Simply issue the `gradle build` task. A `build/libs/MSET.jar` file should be created. 
