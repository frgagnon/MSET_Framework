package ca.cyberseclab.avp.mset.testing.framework.impl;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import ca.cyberseclab.avp.mset.testing.framework.AbstractTestingActivity;

/**
 * Created by jeremiep on 2016-06-09.
 */
public abstract class ButtonTestingActivity extends AbstractTestingActivity {
    public ButtonTestingActivity(String tag) {
        super(tag);
    }

    @Override
    protected void viewCreated() {
    }

    private Button createButton() {
        Button btn = new Button(this);
        btn.setText("Press this button");
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                (ButtonTestingActivity.this).buttonClicked();
            }
        });

        return btn;
    }

    protected void buttonClicked()
    {
        Log.d(activityTag, "button has been clicked");
        this.executeTest();
    }

    @Override
    protected final Object getContentView() {
        RelativeLayout relLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );

        Button btn = this.createButton();

        RelativeLayout.LayoutParams blp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
        );
        blp.addRule(RelativeLayout.CENTER_IN_PARENT);

        btn.setLayoutParams(blp);

        relLayout.addView(btn);

        return relLayout;
    }
}
