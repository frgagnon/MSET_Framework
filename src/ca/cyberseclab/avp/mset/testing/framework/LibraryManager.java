package ca.cyberseclab.avp.mset.testing.framework;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by jeremiep on 2016-06-10.
 */
public class LibraryManager {
    private static LibraryManager instance = null;
    public static LibraryManager getInstance() {
        if(instance == null) {
            instance = new LibraryManager();
        }

        return instance;
    }

    private LinkedList<Class<? extends AbstractTestingActivity>> registeredActivities = new LinkedList<>();

    private LibraryManager() {

    }

    public void registerActivity(Class<? extends AbstractTestingActivity> act) {
        this.registeredActivities.add(act);
    }

    List<Class<? extends AbstractTestingActivity>> getRegisteredActivities() {
        return this.registeredActivities;
    }
}
