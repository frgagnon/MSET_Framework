package ca.cyberseclab.avp.mset.testing.framework;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by jeremiep on 2016-06-09.
 */
public class EvasionTestTriggerReceiver extends BroadcastReceiver {
    private static final String TAG = "EVASION_TEST_TRIGGER";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "MSET Test has been triggered");
        for(Class<? extends AbstractTestingActivity> act : LibraryManager.getInstance().getRegisteredActivities()) {
            context.startActivity(new Intent(context, act));
        }
    }
}
