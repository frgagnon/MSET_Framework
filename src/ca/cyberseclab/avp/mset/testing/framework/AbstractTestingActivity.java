package ca.cyberseclab.avp.mset.testing.framework;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


/**
 * Created by jeremiep on 2016-06-09.
 */
public abstract class AbstractTestingActivity extends Activity {
    public static final String INTENT_MSET_TEST_COMPLETED = "ca.cyberseclab.avp.mset.MSET_TEST_COMPLETED";
    public static final String INTENT_MSET_TEST_TRIGGER = "ca.cyberseclab.avp.mset.MSET_TRIGGER_TEST";
    public static final String INTENT_MSET_EVADED_EXTRA = "MSET_TEST_COMPLETED_MALWARE_EVADED";

    protected String activityTag = "";

    public AbstractTestingActivity(String tag) {
        this.activityTag = tag;
        LibraryManager.getInstance().registerActivity(this.getClass());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((View) this.getContentView());
        this.executeTest();
    }

    protected void viewCreated() {}
    protected abstract Object getContentView();
    protected abstract void executeEvasionDetection();

    protected void fireEvadedEvent(boolean evaded) {
    	// signal that test has completed with the result
        Intent completed = new Intent(INTENT_MSET_TEST_COMPLETED);
        completed.putExtra(INTENT_MSET_EVADED_EXTRA, evaded);
        sendBroadcast(completed);
    }
    
    protected void executeTest()
    {
        this.executeEvasionDetection();
    }
}
