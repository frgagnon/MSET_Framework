package ca.cyberseclab.avp.mset.testing.framework;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by jeremiep on 2016-06-09.
 */
public class EvasionTestResultReceiver extends BroadcastReceiver {

    private static final String TAG = "EVASION_TEST_RESULT";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(!intent.hasExtra(AbstractTestingActivity.INTENT_MSET_EVADED_EXTRA)) {
            Log.e(TAG, "Invalid " + AbstractTestingActivity.INTENT_MSET_TEST_COMPLETED + " intent received.");
            return;
        }
        
        boolean evaded = intent.getBooleanExtra(AbstractTestingActivity.INTENT_MSET_EVADED_EXTRA, false);
        Log.d(TAG, "Evasion test has completed; evaded = " + evaded);
        Toast.makeText(context, "Evasion test completed; evaded = " + evaded, 3000).show();;
    }
}
