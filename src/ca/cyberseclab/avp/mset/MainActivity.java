package ca.cyberseclab.avp.mset;

import android.util.Log;

import ca.cyberseclab.avp.mset.testing.framework.impl.ButtonTestingActivity;

public class MainActivity extends ButtonTestingActivity {
    private static final String TAG = "MSET_BUTTON_APP";

    public MainActivity() {
        super(TAG);
    }

    @Override
    protected void executeEvasionDetection() {
        Log.d(TAG, "Trivially evading sandbox");
        this.fireEvadedEvent(true);
    }
}
